## Este repositorio contiene un ejemplo de manejo de usuarios en Laravel

### Para que el sistema funcione se deben seguir los siguientes pasos

Actualizar composer ``composer update``

Crear una base de datos con el nombre laravelUsuarios

Modificar el archivo ``.env`` , agregar usuario y contraseña de base de datos

Generar llave ``php artisan key:generate``  

Generar migraciones ``php artisan migrate``

Sembrar base de datos ``php artisan db:seed``

Levantar servidor ``php artisan serve``

Email de usuario ``admin@admin.com``

Contraseña ``password``