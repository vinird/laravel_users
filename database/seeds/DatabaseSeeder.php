<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('rols')->insert([
        	'id' => 1,
        	'nombre' => 'Administrador'
        	]);

        DB::table('rols')->insert([
        	'id' => 2,
        	'nombre' => 'Invitado'
        	]);

        DB::table('users')->insert([
        	'name'     => 'Michael',
        	'apellido' => 'Rodriguez',
        	'email'    => 'admin@admin.com',
        	'password' => bcrypt('password'),
        	'rol'      => 1,
        	]);

        DB::table('users')->insert([
        	'name'     => 'Invitado',
        	'apellido' => 'Apellido',
        	'email'    => 'admin@admin.comm',
        	'password' => bcrypt('password'),
        	'rol'      => 2,
        	]);

        for ($i=0; $i < 500; $i++) { 
        	DB::table('users')->insert([
        	'name'     => str_random(10),
        	'apellido' => str_random(20),
        	'email'    => str_random(15),
        	'password' => str_random(6),
        	'rol'      => 1,
        	]);
        }

        for ($i=0; $i < 500; $i++) { 
        	DB::table('users')->insert([
        	'name'     => str_random(10),
        	'apellido' => str_random(20),
        	'email'    => str_random(15),
        	'password' => str_random(6),
        	'rol'      => 2,
        	]);
        }
    }
}
