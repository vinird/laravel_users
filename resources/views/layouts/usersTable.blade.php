@if(isset($users))
@if(count($users))
<table class="table table-hover">
	<thead>
		<th>Id</th>
		<th>Nombre</th>
		<th>Apellido</th>
		<th>Email</th>
		<th>Rol</th>
		<th></th>
		<th></th>
	</thead>
	<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ $user->id }}</td>
				<td>{{ $user->name }}</td>
				<td>{{ $user->apellido }}</td>
				<td>{{ $user->email }}</td>
				<td>
					@if(isset($rols))
						@foreach($rols as $rol)
							@if($rol->id == $user->rol)
								{{ $rol->nombre }}
							@endif
						@endforeach
					@endif
				</td>
				<td><a href="{{ url('users/'.$user->id.'/edit') }}" class="btn btn-warning btn-sm">Modificar</a></td>
				<td>
					<form action="{{ url('users/'.$user->id) }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">
						<input type="submit" value="Eliminar" class="btn btn-danger btn-sm">
					</form>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
<div class="center-block text-center">
	{{ $users->links() }}
</div>
@endif
@endif