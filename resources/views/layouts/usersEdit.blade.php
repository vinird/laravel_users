@if(isset($user))
<form class="form-horizontal" action="{{ url('users/'.$user->id) }}" method="POST">
	{{ csrf_field() }}
	<input type="hidden" name="_method" value="PUT">

	<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
		<label for="nombre" class="col-sm-2 control-label">Nombre</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="nombre" value="{{ $user->name }}">
			@if($errors->has('nombre'))
				<span class="help-block">
					<strong>{{ $errors->first('nombre') }}</strong>
				</span>
			@endif
		</div>
	</div>

	<div class="form-group {{ $errors->has('apellido') ? 'has-error' : '' }}">
		<label for="apellido" class="col-sm-2 control-label">Apellido</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="apellido" value="{{ $user->apellido }}">
			@if($errors->has('apellido'))
				<span class="help-block">
					<strong>{{ $errors->first('apellido') }}</strong>
				</span>
			@endif
		</div>
	</div>

	<div class="form-group {{ $errors->has('rol') ? 'has-error' : '' }}">
		<label for="apellido" class="col-sm-2 control-label">Apellido</label>
		<div class="col-sm-10">
			@if(isset($rols))
			<select class="form-control" name='rol'>
				@foreach($rols as $rol)
				<option value="{{ $rol->id }}" >{{ $rol->nombre }}</option>
				@endforeach
			</select>
			@if($errors->has('rol'))
				<span class="help-block">
					<strong>{{ $errors->first('rol') }}</strong>
				</span>
			@endif
			@endif
		</div>
	</div>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Email</label>
		<div class="col-sm-10">
			<input type="email" class="form-control" value="{{ $user->email }}" disabled>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-warning btn-sm">Modificar</button>
		</div>
	</div>
</form>
@endif