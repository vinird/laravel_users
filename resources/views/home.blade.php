@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if(isset($userEdit))
                        @if($userEdit)
                            @include('layouts.usersEdit')
                        @endif
                    @endif

                    @if(isset($userTable))
                        @if($userTable)

                            @include('layouts.usersTable')

                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
